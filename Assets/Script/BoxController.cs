﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoxController : MonoBehaviour
{
    public UIManager uIManager;
    public RequestController requestController;
    public InputManager ınputManager;
    public colorsType colorsType;
    Vector3 endpos;

    [SerializeField] public List<colorsType> allType;

    // Start is called before the first frame update
    void Start()
    {

        requestController = FindObjectOfType<RequestController>();
        ınputManager = FindObjectOfType<InputManager>();
        uIManager = FindObjectOfType<UIManager>();
        endpos = new Vector3(1, -12f, 18f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    internal void Jump()
    {
        bool upgradeScore = false;
        foreach (var item in requestController.ActiveColorsType)
        {
            if (allType[item]==colorsType)
            {
                upgradeScore = true;
                break;
            }
            else
            {
                upgradeScore = false;
            }
           
        }
        if (upgradeScore==true)
        {


            uIManager.Scorepositive();

        }
        else
        {

            uIManager.ScoreNegative();


        }
        
        gameObject.transform.DOJump(endpos, 2, 1, 1.3f);
    }
}
