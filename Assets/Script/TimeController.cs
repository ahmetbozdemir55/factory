﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TimeController : MonoBehaviour
{
    public UIManager uIManager;
    public ConveyorBelt conveyorBelt;
    float time;
    public TextMeshProUGUI timeText;
    int randomindex;
    int randomindex1;
    int randomindex2;
    int clamp;
    int clamp1;
    int clamp2;
    public List<int> ClampList;
    [Header("UI References")]
    [SerializeField] private Image Fill_Image;
    // Start is called before the first frame update
    void Start()
    {
        randomindex = Random.Range(0, ClampList.Count);
        clamp = ClampList[randomindex];
        ClampList.Remove(clamp);
        randomindex1 = Random.Range(0, ClampList.Count);
        clamp1 = ClampList[randomindex1];
        ClampList.Remove(clamp1);
        randomindex2 = Random.Range(0, ClampList.Count);
        clamp2 = ClampList[randomindex2];
        ClampList.Remove(clamp2);
        Debug.Log("Clamp:   " + clamp + "Clamp1:   " + clamp1 + "Clamp2:    " + clamp2);
        time = 30;
        timeText.text = "" +(int) time;
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if (clamp == (int)time )
        {
            Debug.Log("Clamp:   " + clamp + "time:   " + (int)time);
            Invoke("BeltSpeedPositive", 1f);

        }
        if (clamp1 == (int)time)
        {
            Invoke("BeltSpeedPositive", 1f);
            Debug.Log("Clamp1:   " + clamp1 + "time:   " + (int)time);
        }
        if (clamp2 == (int)time)
        {
            Invoke("BeltSpeedPositive", 1f);
            Debug.Log("Clamp2:   " + clamp2 + "time:   " + (int)time);
        }
        timeText.text = "" + (int)time;
        if (time <= 0)
        {
            uIManager.FinishGame();
        }
        float newProgressValue = Mathf.InverseLerp(0, 30, time);
        UbdateFillmage(newProgressValue);
    }
    private void UbdateFillmage(float value)
    {
        Fill_Image.fillAmount = value;
    }
    public void BeltSpeedPositive ()
    {
        conveyorBelt.speed = 5f;
        Invoke("BeltSpeed", 2f);

    }
    public void BeltSpeed()
    {
        conveyorBelt.speed = 2f;

    }
}
