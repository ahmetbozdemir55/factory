﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;
    private Dictionary<string, List<GameObject>> pooledObjects;


    public List<GameObject> CubeList;
    private void Awake()
    {
        instance = this;
        pooledObjects = new Dictionary<string, List<GameObject>>();
        //if (instance == null)
        //{
           
        //}
    }
    public void Start()
    {

        for (int j = 0; j < CubeList.Count; j++)
        {
            string selectedCubeName = CubeList[j].name;
            for (int i = 0; i < 5; i++)
            {
                if (!pooledObjects.ContainsKey(selectedCubeName))
                {
                    pooledObjects[selectedCubeName] = new List<GameObject>();
                }

                var instance = Instantiate(Resources.Load(selectedCubeName)) as GameObject;
                pooledObjects[selectedCubeName].Add(instance);

                instance.gameObject.SetActive(false);
            }
        }
    }
    public GameObject GetObject(string name)
    {
        if (!pooledObjects.ContainsKey(name))
        {
            pooledObjects[name] = new List<GameObject>();
        }
        for (int i = 0; i < pooledObjects[name].Count; i++)
        {
            if (!pooledObjects[name][i].activeInHierarchy)
            {
                return pooledObjects[name][i];
            }
        }
        var instance = Instantiate(Resources.Load(name)) as GameObject;
        pooledObjects[name].Add(instance);
        return instance;
    }

    public void ClearObjects(string name)
    {
        if (!pooledObjects.ContainsKey(name))
        {
            return;
        }
        for (int i = 0; i < pooledObjects[name].Count; i++)
        {
            pooledObjects[name][i].SetActive(false);
        }
    }
}