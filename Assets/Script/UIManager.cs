﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameManager gameManager;
    public BoxController boxController;
    public SpawnManager spawnManager;



    [Header("Panels")]
    public GameObject menuPanel;
    public GameObject gamePanel;
    public GameObject finishPanel;


    [Header("Game Panel")]
    public TextMeshProUGUI scoreText;


    public int playerScore;
    private void Start()
    {
        playerScore = 0;
        boxController = FindObjectOfType<BoxController>();
        spawnManager = FindObjectOfType<SpawnManager>();
    }


    public void StartGame()
    {
        HideAllPanel();
        gamePanel.SetActive(true);

        gameManager.StartGame(); 
        if (gameManager.gameStatus == GameStatus.start)
        {
            spawnManager.Delay();
        }
    }

    public void ClickCloseButton()
    {
        HideAllPanel();
        menuPanel.SetActive(true);
        gameManager.gameStatus = GameStatus.menü;
    }
    public void Scorepositive()
    {
        playerScore++;
        ScoreText();
    }
    public void ScoreNegative()
    {
        playerScore = playerScore - 2;
        ScoreText();
    }
    public void ScoreText()
    {

        scoreText.text = "Score:" + (playerScore).ToString();


        if (playerScore >= 10)
        {
            FinishGame();
        }

    }

    public void RestartGame()
    {
        HideAllPanel();

        gameManager.RestartGame();

    }

    public void FinishGame()
    {
        HideAllPanel();
        finishPanel.SetActive(true);
        gameManager.GameFinish();

    }

    public void HideAllPanel()
    {
        menuPanel.SetActive(false);
        gamePanel.SetActive(false);
        finishPanel.SetActive(false);

    }
}
