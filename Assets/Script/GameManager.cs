﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public enum colorsType
{
    Black,
    Blue,
    White,
    Purple,
    Green,
    Pink,
    Red,
    Yellow
}
public enum GameStatus
{
    menü,
    start,
    finish
}
public class GameManager : MonoBehaviour
{
    public GameStatus gameStatus;
    // Start is called before the first frame update
    void Start()
    {
        gameStatus = GameStatus.menü;
    }
    public void StartGame()
    {
        gameStatus = GameStatus.start;
    }
    public void GameFinish()
    {
        gameStatus = GameStatus.finish;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
