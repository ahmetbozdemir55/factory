﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameManager gameManager;
    public GameStatus gameStatus;


    public List<GameObject> CubeList;
    int SelectedCube;
    public void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SpawnCube()
    {
        SelectedCube = Random.Range(0, CubeList.Count);
        ObjectPooler.instance.GetObject(CubeList[SelectedCube].name);
    }
    public void SpawnCubeActive()
    {

        string selectedCubeName = CubeList[Random.Range(0,CubeList.Count)].name;//random aldığım bir prfebların stringlerini çevirdim ve getobject e name olarak gönderdim
        GameObject g = ObjectPooler.instance.GetObject(selectedCubeName);
        g.gameObject.SetActive(true);
        Invoke("Delay", 1f);
        g.transform.position = new Vector3(-3.1f, -11.7f, 15.1f);

    }
    public void Delay()
    {
        Invoke("SpawnCubeActive", 1f);
    }
}
