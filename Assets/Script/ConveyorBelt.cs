﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField]
    public float speed;
    float matrielSpeed=.1f;

    MeshRenderer meshRenderer;

    float yOffest=0;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        meshRenderer = GetComponent<MeshRenderer>();
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 pos = rb.position;
        rb.position -= transform.forward * Time.fixedDeltaTime * speed;
        rb.MovePosition(pos);

        yOffest += Time.fixedDeltaTime * matrielSpeed * speed;
        meshRenderer.material.mainTextureOffset = new Vector2(0, yOffest);
    }
}
