﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FinishPanelScirpt : MonoBehaviour
{
    public BoxController boxController;
    public UIManager uIManager;
    public Button RestartButton;
    public Button Confeti;
    public TextMeshProUGUI confetiText;
    // Start is called before the first frame update
    void Start()
    {
        boxController = FindObjectOfType<BoxController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (uIManager.playerScore<=0)
        {
            RestartButton.gameObject.SetActive(true);
        }
        else
        {
            Confeti.gameObject.SetActive(true);
            confetiText.text = "Skor:" + uIManager.playerScore + "Daha iyisini yapabilirsin";
            confetiText.gameObject.SetActive(true);
        }
    }
}
