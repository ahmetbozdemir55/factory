﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RequestController : MonoBehaviour
{
    public Image Request1;
    public Image Request2;
    public List<Color> RequeatColors;
    [SerializeField] public List<colorsType> allType;
    [SerializeField] public List<int> ActiveColorsType; 

    // Start is called before the first frame update
    void Start()
    {
        ActiveColorsType = new List<int>();
        SetRequest();
    }

    private void SetRequest()
    {

        var randomIndex = Random.Range(0, RequeatColors.Count);
        Request1.color = RequeatColors[randomIndex];
        //RequeatColors.Remove(RequeatColors[randomIndex]);
        List<int> listNumbers = new List<int>();
        int number;
        int randomIndex2;
        listNumbers.Add(randomIndex);
        do
        {
           randomIndex2 = Random.Range(0, RequeatColors.Count);
        } while (!listNumbers.Contains(randomIndex));

        Request2.color = RequeatColors[randomIndex2];

        //RequeatColors.Remove(RequeatColors[randomIndex2]);
        SetActiveType(randomIndex);
        SetActiveType(randomIndex2);
    }

    private void SetActiveType(int randomIndex)
    {
        switch (allType[randomIndex])
        {
            case colorsType.Black:
                ActiveColorsType.Add(0);

                //allType.Remove(colorsType.Black);
                break;
            case colorsType.Blue:
                ActiveColorsType.Add(1);

                //allType.Remove(colorsType.Blue);
                break;
            case colorsType.Green:
                ActiveColorsType.Add(2);

                //allType.Remove(colorsType.Green);
                break;
            case colorsType.Pink:
                ActiveColorsType.Add(2);

                //allType.Remove(colorsType.Pink);
                break;
            case colorsType.Purple:
                ActiveColorsType.Add(4);

                //allType.Remove(colorsType.Purple);
                break;
            case colorsType.Red:
                ActiveColorsType.Add(5);

                //allType.Remove(colorsType.Red);
                break;
            case colorsType.White:
                ActiveColorsType.Add(6);

                //allType.Remove(colorsType.White);
                break;
            case colorsType.Yellow:
                ActiveColorsType.Add(7);

                //allType.Remove(colorsType.Yellow);
                break;

        }

    }
         

    // Update is called once per frame
    void Update()
    {
        
    }
}
